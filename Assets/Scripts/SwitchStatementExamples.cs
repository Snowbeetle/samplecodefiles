using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchStatementExamples : MonoBehaviour
{
	[SerializeField]
	private int testInt = 30;
	public int TestInt => testInt;

	[SerializeField]
	private string testString = "HI";
	public string TestString => testString;

	/// <summary>
	/// Start is called before the first frame update. It'll demonstrate how the switch statements work via which logs get printed out.
	/// </summary>
	void Start()
	{
		// See also for further examples/explanations: https://stackoverflow.com/a/44078916

		// First example: a slightly more complex condition in a case than just comparing against a const value (like 'case "hi":')
		switch (testString)
		{
			// Do an operation that returns a bool based on the testString value; in this case, a case-insensitive comparison against the const string "hi".
			// The variable declared before the 'when' is populated with the value of whatever is within the switch(*) above (so testString's value).
			case var scopedTestStringInstance when "hi".Equals(scopedTestStringInstance, StringComparison.OrdinalIgnoreCase):
				Debug.Log(
					"This will get hit if we're using the default value of testString as HI == hi when doing an ordinal ignore case comparison.");
				break;
			default:
				Debug.Log(
					"This won't get hit if we're using the default value of testString, because the above case will.");
				break;
		}

		switch (testInt)
		{
			// Some different syntaxes for things like range checks.

			// After 'when' can be pretty much any valid C# code that returns a bool, much like the 'Equals' case above with the string example.
			case var scopedTestIntInstance when scopedTestIntInstance < 0 && scopedTestIntInstance >= -30:
				Debug.Log(
					"This won't get hit when using the default value of testInt because we're == 30, not within the range [-30,0).");
				break;

			/*
			 The below cases within this comment block don't work because Unity isn't at C# 9.0 yet. You'll have to use the syntax above for now, but for future reference:
			 
			// There's also some special syntax/shorthand for evaluating against multiple comparisons with 'is' and 'and'.
			case var scopedTestIntInstance when scopedTestIntInstance is >= 0 and < 30:
				Debug.Log(
					"This won't get hit when using the default value of testInt because we're == 30, not within the range [0, 30).");
				break;

			// And even shorter-hand if we're just doing simple comparisons to the switch value.
			case > 30 and < 60:
				Debug.Log("This means the same thing as above, but for the range (30, 60) instead of [0,30).");
				break;
			*/

			// And, of course, the traditional uses of 'case' and 'default' still work.
			case 30:
				Debug.Log(
					"This traditional case should be what gets hit if testInt is at the default value because we're == 30");
				break;
			default:
				Debug.Log(
					"This shouldn't get hit if testInt is at the default value because it isn't outside the above ranges.");
				break;
		}

		switch (testInt)
		{
			// You can also rely on 'break' to get you out early as you go through the cases in-order to minimize comparisons.
			// Just have to remember to add new cases in the right order if this is the approach you take.
			// Once C# 9.0 is around, you can just do "case >= 10000:", for example, but for now we need the longer format.
			case var scopedTestIntInstance when scopedTestIntInstance >= 10000:
				Debug.Log("testInt was 10000 or more!");
				break;
			case var scopedTestIntInstance when scopedTestIntInstance >= 5000:
				Debug.Log("testInt was in the range [5000,10000)");
				break;
			case var scopedTestIntInstance when scopedTestIntInstance >= 30:
				Debug.Log("testInt was in the range [30, 5000)");
				break;
			case var scopedTestIntInstance when scopedTestIntInstance >= 0:
				// This won't get hit if testInt is at the default value of 30 because we'll hit case >= 30 above and break.
				Debug.Log("testInt was in the range [0, 30)");
				break;
			case var scopedTestIntInstance when scopedTestIntInstance < 0:
				Debug.Log("testInt was less than 0.");
				break;
			default:
				// There should be no valid values that could result in this, because the above cases cover [int.MinValue,int.MaxValue]
				Debug.Log("received an invalid int value");
				break;
		}

		// Box testInt in an object to demonstrate the type check part of this 'case' syntax. Switch against the object-boxed variable.
		object testIntAsObject = testInt;
		switch (testIntAsObject)
		{
			// This switch syntax also handles checking type assignability of the switch value (which is part of why the scoped variable instance is part of the syntax)
			case long scopedTestIntAsObjectInstance when scopedTestIntAsObjectInstance == 30:
				Debug.Log(
					"This won't get hit because testIntAsObject is actually holding an int, not a long, so it fails the 'long' part of the case!");
				// And no, the fact that you can safely cast an int to a long doesn't matter; the distinction between being assignable to type 'long' as opposed to type-convertible to type 'long' is important here.
				break;

			case int scopedTestIntAsObjectInstance when scopedTestIntAsObjectInstance == 30:
				Debug.Log(
					"This will get hit if testInt is at the default value because testIntAsObject is really holding an int of value 30!");
				break;

			case var scopedTestIntAsObjectInstance when "30".Equals(scopedTestIntAsObjectInstance.ToString()):
				Debug.Log(
					"The use of 'var' here means the case isn't actually doing a type check/cast and is just using the switch variable as the type passed in (so 'object', in this case), " +
					"meaning we can only treat scopedTestIntAsObjectInstance as an object (unless we manually cast it back to an int or something).");
				// This case doesn't get hit because the 'int' case above *does*, and that case breaks out of the switch statement. If that case didn't exist, this case would be a match if testInt was 30.
				break;
			
			// We also have the option of discarding the scoped variable by having the name be '_' and just referencing the variable passed into the switch statement directly.
			// This has the downside of: if a *different* variable was passed to the switch call (if someone changed it to "switch(testString)", for example),
			// we'd still be evaluating this case based on "testIntAsObject"'s value rather than what's actually being switched. Not recommended as a result.
			case var _ when "29".Equals(testIntAsObject.ToString()):
				Debug.Log("Similar to the above case, this will match whenever testIntAsObject.ToString() resolves to be \"29\".");
				break;

			case int scopedTestIntAsObjectInstance:
				Debug.Log(
					$"This will get hit if testInt was something other than one of the above caught values since we're switching based on a boxed int! It was {scopedTestIntAsObjectInstance}.");
				break;

			default:
				Debug.Log(
					"This case won't get hit as the \"case int scopedTestIntAsObjectInstance:\" case always will, since it's always a boxed int.");
				break;
		}
	}
}
